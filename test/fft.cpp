#include "minilearn/fft.h"
#include "minilearn/mat.h"
#include <iostream>
#include <random>

int main()
{
    namespace ml = minilearn;
    constexpr int N = 512;

    auto eq = [](float a, float b){
        if(abs(a-b) > 0.0001) {
            std::cout << a << " != " << b << std::endl;
            assert(a == b);
            exit(1);
        }
    };

    for(int i=1 ; i<N/4 ; i++)
    {   float s = ml::fftdetail::fftsin<N>(i);
        float c = ml::fftdetail::fftcos<N>(i);
        eq(s, std::sin(i * 2 * M_PI / N));
        eq(c, std::cos(i * 2 * M_PI / N));
    }

    float in[N];
    std::mt19937 rng;
    std::uniform_real_distribution<float> uniform(-1, 1);
    for(int i=0 ; i<N ; i++)
    {   in[i] = uniform(rng);
    }

    ml::vec<ml::complex<float>> out;
    ml::fft<N>(in, out);

    ml::vec<ml::complex<float>> out2;
    ml::slow_dft<N>(in, out2);
    
    for(int i=0 ; i<N ; i++)
    {   eq(out[i].r, out2[i].r);
        eq(out[i].i, out2[i].i);
    }
    std::cout << "success\n";
}