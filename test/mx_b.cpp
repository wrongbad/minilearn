#include "minilearn/mat.h"
#include "minilearn/adam.h"
#include "minilearn/layers.h"

#include <iostream>
#include <random>
#include <cmath>

int main()
{
    std::random_device rd;
    std::mt19937 rng(rd());
    std::uniform_real_distribution<float> uniform(-1, 1);

    namespace ml = minilearn;

    ml::adam::context adam_ctx;
    // Set smaller smoothing masses because
    // this 2-param model can converge quickly
    adam_ctx.m1mass = 10; // 1/(1-Beta1)
    adam_ctx.m2mass = 100; // 1/(1-Beta2)

    ml::adam m = 0;
    ml::adam b = 0;

    auto target = [](float x){ return 5.55f * x + 1.11f; };

    const int train_steps = 1000;
    for(int i=0 ; i<train_steps ; i++)
    {   
        float x = uniform(rng);

        ml::grad predict = m * x + b;

        ml::grad loss = predict - target(x);
        loss = loss * loss;
        loss.compute_gradient();

        adam_ctx.rate = 0.1f * std::exp(-i*5.0f/train_steps);
        m.update(adam_ctx);
        b.update(adam_ctx);

        if(i%(train_steps/10) == 0)
        {   std::cout << "loss: " << loss.val << "\n";
        }
    }
    
    std::cout << "m: " << m.val << "\n";
    std::cout << "b: " << b.val << "\n";

    return 0;
}