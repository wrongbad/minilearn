#include "minilearn/fft.h"
#include "minilearn/mat.h"
#include "minilearn/adam.h"
#include <iostream>
#include <random>

int main()
{
    namespace ml = minilearn;
    constexpr int FIRN = 256;
    constexpr int FFTN = 512;

    ml::svec<ml::adam, FIRN> fir = ml::fill(0);
    fir[0] = 1;

    float fs = 44100;
    float f0 = 440;
    // f[i] = f0 * i * sqrt(1 + a*i*i)
    // del = int(fs / f0)
    // ph[i] = ((fs / f[i])*i - del) / (fs / f[i])
    // ph[i] = (del / (fs / f[i]) % 1) * 2 * pi

    int del = int(fs / f0);

    float inharm = 0.001;
    ml::svec<float, 64> harmonics;
    for(int i=0 ; i<harmonics.size() ; i++)
    {   harmonics[i] = f0 * i * std::sqrt(1 + inharm * i * i);
    }

    ml::svec<ml::complex<float>, FFTN> target;
    for(int i=0 ; i<FFTN/2 ; i++)
    {           
        float initf = i * fs / FFTN;
        int besth = 0;
        for(int h=1 ; h<harmonics.size() ; h++)
        {   if(std::abs(harmonics[h]-initf) < std::abs(harmonics[besth]-initf))
            {   besth = h;
            }
        }
        float phase = del * harmonics[besth] / fs;
        phase -= std::round(phase);
        phase *= 2 * M_PI;
        // std::cout << i << "\t" << phase << "\n";
        target[i] = {std::cos(phase), std::sin(phase)};
        if(i) { target[FFTN-i] = {target[i].r, -target[i].i}; }
    }

    ml::svec<ml::grad, FFTN> in;
    ml::svec<ml::complex<ml::grad>, FFTN> out;
    for(int ep=0 ; ep<10000 ; ep++)
    {
        for(int i=0 ; i<FIRN ; i++) { in[i]=fir[i]; }
        ml::fft<FFTN>(in, out);
        ml::grad loss = 0;
        for(int i=0 ; i<FFTN ; i++)
        {   loss += (out[i]-target[i]).sqmag();
        }
        loss.compute_gradient();

        exec(map(fir, [&](auto& x){ x.update(); }));
    }


    for(int i=0 ; i<FFTN ; i++)
    {   std::cout << target[i] << "\t" << ml::complex<float>(out[i]) << "\n";
    }

    for(int i=0 ; i<FIRN ; i++)
    {   std::cout << i << "\t" << fir[i].val << "\n";
    }

    return 0;
}