# minilearn

A very tiny and readable c++14 header only machine learning toolkit

Based on scalar-level gradient tape

This will be much less efficient than tensor-level gradient frameworks for tensor operations.
On the other hand this can more easily adapt to differentiate arbitrary code.

* Write your model's function once in c++ and use for both training and inference
* Adam optimizer included, other optimizers could be implemented easily
* Tiny templated vector/matrix math library with generic programming adapters
* Crude but workable binary (de)serialization included
* Templated FFT and Complex types included (both differentiable)
* Entire library is about 1000 lines, split in 10 header files

Run some test targets:
```
make tests
make test/tiny_dnn
make debug/test/tiny_dnn
```

Use the library (from test/tiny_dnn.cpp):
```cpp
#include "minilearn/adam.h"
#include "minilearn/layers.h"
#include "minilearn/train.h"
#include "minilearn/raw.h"

#include <functional>
#include <iostream>
#include <fstream>
#include <random>
#include <cmath>

namespace ml = minilearn;

// Param = float for inference
// Param = ml::adam for training
template<class Param>
struct Model
{   ml::linear<Param> layer0 {1, 128};
    ml::linear<Param> layer1 {128, 128};
    ml::linear<Param> layer2 {128, 1};

    // inference computation
    template<class V>
    auto operator()(V in) const
    {   ml::svec<V,1> invec {in};
        auto tmp0 = eval(map(layer0(invec), ml::softsign()));
        auto tmp1 = eval(map(layer1(tmp0), ml::softsign()));
        auto outvec = eval(map(layer2(tmp1), ml::softsign()));
        return outvec[0];
    }

    // visit every parameter with function f
    template<class F>
    void apply(F && f)
    {   layer0.apply(f);
        layer1.apply(f);
        layer2.apply(f);
    }
};

int main()
{   
    // we're going to try to predict the output of this function
    auto target = [](float x){ return 0.5f*std::exp(-16*x*x); };

    // test on equally spaced samples
    auto test_data = map(ml::range(100), [&](int i){
        float x = { -1 + i / 50.0f };
        return std::make_pair(x, target(x));
    } );
    
    // train on random samples
    auto train_data = [&](int epoch){
        ml::xorshift rng(epoch);
        std::uniform_real_distribution<float> dist(-1, 1);
        return map(ml::range(1000), [&, rng, dist] (int i) mutable {
            float x = dist(rng);
            return std::make_pair(x, target(x));
        });
    };

    auto loss = ml::squared_error();

    // create model with "adam" parameters
    Model<ml::adam> model;

    // randomize initial conditions
    auto rng = ml::gaussian_rng();
    model.apply([&](auto& x){ x = rng(); });

    // set training parameters
    ml::adam::context adam_ctx;
    adam_ctx.m1mass = 10; // 1/(1-Beta1)
    adam_ctx.m2mass = 1000; // 1/(1-Beta2)

    // train
    for(int epoch=0 ; epoch<10 ; epoch++)
    {   std::cout << "loss: " << ml::test(model, test_data, loss) << std::endl;
        adam_ctx.rate = 0.01 * std::exp(epoch * -0.6);
        ml::train(model, train_data(epoch), loss, adam_ctx);
    }
    std::cout << "loss: " << ml::test(model, test_data, loss) << std::endl;

    // save model to file
    std::ofstream out("tiny_dnn_params.bin", std::ios::binary);
    model.apply([&](auto& x){ out << ml::raw<float>(x); });
    out.close();

    // load model back, this time with simple float parameters
    Model<float> model2;
    std::ifstream in("tiny_dnn_params.bin", std::ios::binary);
    model2.apply([&](auto& x){ in >> ml::raw<float>(x); });
    in.close();

    // run inference model, without gradient overhead
    float final_loss = ml::test(model2, test_data, loss);
    std::cout << "read-back model loss: " << final_loss << std::endl;

    bool test_fail = final_loss > 0.0002;
    return test_fail;
}
```