#pragma once

#include "minilearn/grad.h"
#include "minilearn/vec.h"

#include <random>
#include <future>
#include <iostream>

namespace minilearn {

struct xorshift // faster rng
{   uint64_t x;
    xorshift(uint64_t s0=0)
    :   x(s0)
    {   (*this)(); (*this)(); (*this)();
    }
    xorshift(uint32_t s0, uint32_t s1)
    :   xorshift((uint64_t(s0)<<32) | s1)
    {}
    uint64_t operator()()
    {   x ^= ~(x << 13);
        x ^= x >> 7;
        x ^= x << 17;
        return x;
    }
    static constexpr uint64_t min() { return 0; }
    static constexpr uint64_t max() { return UINT64_MAX; }
};

auto gaussian_rng(float mean = 0, float stdev = 1, uint64_t seed = 0)
{   return [ rng = xorshift(seed),
        dist = std::normal_distribution<float>(mean, stdev)]
        () mutable { return dist(rng); };
}

auto mean_squared_error()
{   return [](auto && predict, auto && label){
        return avg(squares(predict-label));
    };
}
auto squared_error() // scalar version
{   return [](auto && predict, auto && label){
        auto d = predict-label;
        return d * d;
    };
}

template<class Model, class Data, class Loss, class... Context>
void train(Model && model, Data && data, Loss && loss, Context &&... ctx)
{   exec( map(data, [&](auto && p){
        loss(model(p.first), p.second).compute_gradient();
        model.apply([&](auto & x){ x.update(ctx...); });
    } ) );
}

// TODO lots of problems with this
// -Stateful models broken
// -Stateful data generaters broken
// -Gradient pass not thread safe due to float+= at adam::own_node::grad
// template<class Model, class Data, class Loss, class... Context>
// void train_multi(size_t threads, Model && model, Data const&& data, Loss const&& loss, Context &&... ctx)
// {   std::mutex sync;
//     std::vector<std::future<void>> jobs; // destructor joins threads
//     for(size_t t=0 ; t<threads ; t++)
//     {   jobs.push_back( std::async( std::launch::async, [&, t] {
//             for(int i=t ; i<data.size() ; i+=threads)
//             {   auto l = loss(model(data[i].first), data[i].second);
//                 {   std::lock_guard<std::mutex> lock(sync);
//                     l.compute_gradient(); // TODO this is mostly parallelizable
//                     model.apply([&](auto & x){ x.update(ctx...); });
//                 }
//             }
//         } ) );
//     }
// }

template<class Model, class Data, class Loss>
auto test(Model && model, Data && data, Loss && loss)
{   return avg( map(data, [&](auto && p){ 
        return (float)loss(model(p.first), p.second);
    } ) );
}

} // minilear   n