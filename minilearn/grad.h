#pragma once

#include <vector>
#include <cmath>
#include <ios>
#include <deque>
#include <iostream>

namespace minilearn {

struct grad
{   
    struct Node
    {   Node * n0;
        Node * n1;
        float w0;
        float w1;
        float grad;
    };

    struct Tape
    {   std::deque<Node> nodes;
        void clear() { nodes.clear(); }
        Node * push(Node n)
        {   nodes.push_back(n);
            return &nodes.back();
        }
        // iterate backwards
        auto begin() { return nodes.rbegin(); }
        auto end() { return nodes.rend(); }
    };

    static Tape & tape()
    {   static thread_local Tape _tape;
        return _tape;
    }

    static Node * push(float w0 = 0, Node * n0 = nullptr, float w1 = 0, Node * n1 = nullptr)
    {   return tape().push({n0, n1, w0, w1, 0});
    }

    float val;
    Node * node;

    grad(float _val = 0, Node * _node = nullptr)
    :   val(_val), node(_node)
    {}
    grad & operator=(float v) { return *this = grad(v); }
    explicit operator float() const { return val; }

    grad operator+(grad const& b) const
    {   return {val + b.val, push(1, node, 1, b.node)};
    }
    grad operator-(grad const& b) const
    {   return {val - b.val, push(1, node, -1, b.node)};
    }
    grad operator*(grad const& b) const
    {   return {val * b.val, push(b.val, node, val, b.node)};
    }
    grad operator/(grad const& b) const
    {   return {val / b.val, push(1/b.val, node, -val/(b.val*b.val), b.node)};
    }
    grad operator-() const { return {-val, push(-1, node)}; }
    grad & operator+=(grad const& b) { return *this = *this + b; }
    grad & operator-=(grad const& b) { return *this = *this - b; }
    grad & operator*=(grad const& b) { return *this = *this * b; }
    grad & operator/=(grad const& b) { return *this = *this / b; }

    void compute_gradient(bool keep = false, float scale = 1)
    {   assert(node);
        node->grad += scale;
        for(Node & n : tape())
        {   if(n.n0) { n.n0->grad += n.grad * n.w0; }
            if(n.n1) { n.n1->grad += n.grad * n.w1; }
            n.grad = 0;
        }
        if(!keep) { tape().clear(); }
    }
};

inline grad operator+(float a, grad const& b) { return grad(a) + b; }
inline grad operator-(float a, grad const& b) { return grad(a) - b; }
inline grad operator*(float a, grad const& b) { return grad(a) * b; }
inline grad operator/(float a, grad const& b) { return grad(a) / b; }
inline grad operator+(grad const& a, float b) { return a + grad(b); }
inline grad operator-(grad const& a, float b) { return a - grad(b); }
inline grad operator*(grad const& a, float b) { return a * grad(b); }
inline grad operator/(grad const& a, float b) { return a / grad(b); }

template<class Stream>
Stream & operator<<(Stream && out, grad const& a)
{   return out << a.val;
}

} // minilearn

namespace std {

inline minilearn::grad abs(minilearn::grad const& a) 
{   return (a.val >= 0) ? a : -a;
}
inline minilearn::grad log(minilearn::grad const& a)
{   return {std::log(a.val), minilearn::grad::push(1/a.val, a.node)};
}
inline minilearn::grad exp(minilearn::grad const& a)
{   return {std::exp(a.val), minilearn::grad::push(std::exp(a.val), a.node)};
}
inline minilearn::grad sin(minilearn::grad const& a)
{   return {std::sin(a.val), minilearn::grad::push(std::cos(a.val), a.node)};
}
inline minilearn::grad cos(minilearn::grad const& a)
{   return {std::cos(a.val), minilearn::grad::push(-std::sin(a.val), a.node)};
}

} // std