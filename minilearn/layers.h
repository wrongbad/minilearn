#pragma once

#include "minilearn/mat.h"

namespace minilearn {

template< class WeightScalar,
    class Mat = mat<WeightScalar>,
    class Vec = vec<WeightScalar> >
struct linear
{   Mat w;
    Vec b;
    linear(int isize, int osize)
    :   w(isize, osize),
        b(osize)
    {}
    template<class V>
    auto operator()(V && in) const
    {   return std::forward<V>(in) * w + b;
    }
    template<class F>
    void apply(F && f)
    {   exec(map(w, f));
        exec(map(b, f));
    }
};


auto softsign()
{   return [](auto && x){ return x / (std::abs(x) + 1); };
}

} // minilearn