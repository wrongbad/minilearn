#pragma once

#include "minilearn/grad.h"

#include <iostream>

namespace minilearn {

struct adam : public grad
{  
    struct context
    {   int m1mass = 10; // 1/(1-Beta1)
        int m2mass = 1000; // 1/(1-Beta2)
        float rate = 0.001f; // alpha
        float eps = 1e-8f; // avoid div-zero
    };

    grad::Node own_node = {0,0,0,0,0};
    float m1 = 0;
    float m2 = 0;
    int step = 0; // redundant, but less error-prone
    
    // set grad's node* to our own node
    adam(float v = 0) : grad(v, &own_node) {}
    adam(grad const& o) : adam(o.val) {}
    
    adam & operator=(float v) { this->val = v; return *this; }
    adam & operator=(grad const& o) { this->val = o.val; return *this; }

    // allow updating from a different gradient tape
    void update(context const& ctx)
    {   step ++;
        float & grad = own_node.grad;
        // The unbiasing is slightly different from OG Adam
        m1 += (grad - m1) / std::min(ctx.m1mass, step);
        m2 += (grad*grad - m2) / std::min(ctx.m2mass, step);
        val -= ctx.rate * m1 / (std::sqrt(m2) + 1e-8f);
        grad = 0;
    }
    void update() { update(context{}); }
    // void update(context const& ctx = context{})
    // {   update(ptr, id, ctx);
    // }
    // // allow updating from a different gradient tape
    // void update(tape * t, int node, context const& ctx = context{})
    // {   step ++;
    //     float & grad = t->nodes[node].grad;
    //     // std::cout << "grad: " << grad << std::endl;
    //     // The unbiasing is slightly different from OG Adam
    //     m1 += (grad - m1) / std::min(ctx.m1mass, step);
    //     m2 += (grad*grad - m2) / std::min(ctx.m2mass, step);
    //     v -= ctx.rate * m1 / (std::sqrt(m2) + 1e-8);
    //     grad = 0;
    // }
};

} // minilearn