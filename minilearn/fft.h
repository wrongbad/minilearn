#pragma once

#include "minilearn/complex.h"

#include <cmath>
#include <utility>

namespace minilearn {
namespace fftdetail {

inline constexpr bool ispow2(int n)
{   return ((n & (n-1)) == 0) && (n > 0);
}
template<int N>
constexpr int bitreverse(int i)
{   int o=0;
    for(int b=i+N ; b>1 ; b>>=1)
    {   o = (o<<1) | (b&1);
    }
    return o;
}
template<int N, class InVec, class OutVec>
void bitshuffle(InVec && in, OutVec && out)
{   for(int i=0 ; i<N ; i++)
    {   out[bitreverse<N>(i)] = in[i];
    }
}
template<int N, class T>
struct table
{   T v[N];
    static constexpr T sqrt(T val)
    {   T root = (1 + val)/2;
        root = (root + val/root)/2;
        root = (root + val/root)/2;
        return (root + val/root)/2;
    }
    static constexpr void table_fill(T * t, int li, int ri, T lx, T ly, T rx, T ry)
    {   if(ri - li < 2) { return; }
        T mx = lx + rx;
        T my = ly + ry;
        T norm = sqrt(mx * mx + my * my);
        mx /= norm;
        my /= norm;
        int mi = (li+ri)/2;
        t[mi] = my;
        table_fill(t, mi, ri, mx, my, rx, ry);
        table_fill(t, li, mi, lx, ly, mx, my);
    }
    constexpr table()
    {   table_fill(&v[0],0,N,1,0,0,1);
    }
};
template<int N, class T = float>
T fftsin(int i) // sin(i * 2pi / N) : 0 < i < N/4
{   static constexpr table<N/4,T> tab; 
    return tab.v[i];
}
template<int N, class T = float>
T fftcos(int i) // cos(i * 2pi / N) : 0 < i < N/4
{   return fftsin<N,T>(N/4-i);
}

template<int N, class Vec>
void fft_inner(Vec & v, int off=0)
{   if(N <= 1) { return; /*no op*/ }
    fft_inner<N/2>(v, off);
    fft_inner<N/2>(v, off+N/2);
    auto at = [&](int i)->auto&{ return v[off+i]; };
    auto tmp = at(N/2);
    at(N/2) = at(0) - tmp;
    at(0) = at(0) + tmp;
    if(N >= 4)
    {   tmp = at(N*3/4) * complex<float>{0,1};
        at(N*3/4) = at(N/4) - tmp;
        at(N/4) = at(N/4) + tmp;
    }
    for(int i=1 ; i<N/4 ; i++)
    {   complex<float> tw { fftcos<N>(i), fftsin<N>(i) };
        tmp = at(N/2+i) * tw;
        at(N/2+i) = at(i) - tmp;
        at(i) = at(i) + tmp;
        tw.r = -tw.r;
        tmp = at(N-i) * tw;
        at(N-i) = at(N/2-i) - tmp;
        at(N/2-i) = at(N/2-i) + tmp;
    }
}

} // fftdetail

template<int N, class InVec, class OutVec>
void fft(InVec && in, OutVec && out)
{   static_assert(fftdetail::ispow2(N), "only powers of 2 supported");
    out.resize(N);
    fftdetail::bitshuffle<N>(in, out);
    fftdetail::fft_inner<N>(out);
}

// --- testing / debug functions ---

template<int N, class VecIn, class VecOut>
void slow_dft(VecIn && x, VecOut && out)
{   out.resize(N);
    for(int i=0 ; i<N ; i++)
    {   out[i] = 0;
        for(int j=0 ; j<N ; j++)
        {   out[i] += complex<float>{ 
                std::cos(i * j * 2 * M_PI / N), 
                std::sin(i * j * 2 * M_PI / N) } * x[j];
        }
    }
}

template<int N>
void slow_convolve(float const* a, float const* b, float * out)
{   for(int o=0 ; o<N ; o++)
    {   out[o] = 0;
        for(int i=0 ; i<N ; i++)
        {   out[o] += a[(o - i + N) % N] * b[i];
        }
    }
}

template<int N>
void slow_correlation(float const* a, float const* b, float * out)
{   for(int o=0 ; o<N ; o++)
    {   out[o] = 0;
        for(int i=0 ; i<N ; i++)
        {   out[o] += a[(i + o)%N] * b[i];
        }
    }
}

} // minilearn
