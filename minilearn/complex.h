#pragma once

#include <ios>

namespace minilearn {

template<class T> T real(T const& val) { return val; }
template<class T> T imag(T const& val) { return T(0); }

template<class T> struct complex;
template<class T> T real(complex<T> const& val) { return val.r; }
template<class T> T imag(complex<T> const& val) { return val.i; }
template<class T> T real(complex<T> & val) { return val.r; }
template<class T> T imag(complex<T> & val) { return val.i; }
template<class T> T real(complex<T> && val) { return val.r; }
template<class T> T imag(complex<T> && val) { return val.i; }

template<class T>
struct complex
{   T r, i;
    
    complex(T _r = 0, T _i = 0) : r(_r), i(_i) {}
    
    template<class T2>
    complex(complex<T2> const& o) : r(o.r), i(o.i) {}

    template<class Complex2>
    complex operator+(Complex2 && o) const
    {   return {r + real(o), i + imag(o)};
    }
    template<class Complex2>
    complex operator-(Complex2 && o) const
    {   return {r - real(o), i - imag(o)};
    }
    template<class Complex2>
    complex operator*(Complex2 && o) const
    {   return {r * real(o) - i * imag(o), r * imag(o) + i * real(o)};
    }
    complex operator/(T && o) const
    {   return {r / o, i / o};
    }
    template<class Complex2>
    complex operator/(Complex2 && o) const
    {   return *this * o.conj() / o.sqmag();
    }
    complex conj() const { return {r, -i}; }
    complex transpose() const { return {i, r}; }
    T sqmag() const { return r*r + i*i; }
    template<class Complex2>
    complex & operator+=(Complex2 && o) { return *this=*this+o; }
    template<class Complex2>
    complex & operator-=(Complex2 && o) { return *this=*this-o; }
    template<class Complex2>
    complex & operator*=(Complex2 && o) { return *this=*this*o; }
    template<class Complex2>
    complex & operator/=(Complex2 && o) { return *this=*this/o; }
};
template<class Stream, class T>
Stream & operator<<(Stream && out, complex<T> const& a)
{   return out << a.r << std::showpos << a.i << "i" << std::noshowpos;
}

} // minilearn