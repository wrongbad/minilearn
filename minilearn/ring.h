#pragma once

template<class T, int N>
class ring
{
public:
    void shift(int i)
    {   _off = (_off - i%N + N) % N;
    }
    T & operator[](int i)
    {   return _vec[(_off + i%N + N) % N];
    }
    T const& operator[](int i) const
    {   return _vec[(_off + i%N + N) % N];
    }
    int size() const
    {   return N;
    }
private:
    T _vec[N];
    int _off = 0;
};
