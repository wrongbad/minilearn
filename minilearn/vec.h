#pragma once

#include <vector>
#include <array>
#include <cassert>
#include <type_traits>

namespace minilearn {

using std::forward;
using std::declval;

template<class Vec>
using vec_elem_t = std::remove_reference_t<decltype(declval<Vec>()[0])>;

template<class Vec>
using vec_size_t = decltype(declval<Vec>().size());

template<class Vec, class... Dummy>
int vec_size(Vec&&, Dummy...) { return -1; }

template<class Vec>
vec_size_t<Vec> vec_size(Vec && v) { return v.size(); }

template<class Src, class Dst>
void vec_copy(Src && src, Dst && dst, int n = -1)
{   if(n == -1) { n = vec_size(dst); }
    for(int i=0 ; i<n ; i++) { dst[i] = src[i]; }
}

template<class T>
struct vec : public std::vector<T>
{
    using std::vector<T>::vector;

    template<class Vec, class sfinae = decltype(T(vec_elem_t<Vec>()))>
    vec(Vec && o)
    {   *this = forward<Vec>(o);
    }
    template<class Vec, class sfinae = decltype(T(vec_elem_t<Vec>()))>
    vec & operator=(Vec && o)
    {   if(vec_size(o) != -1) { this->resize(vec_size(o)); }
        vec_copy(o, *this, this->size());
        return *this;
    }
    template<class Vec>
    vec & operator+=(Vec && o) { return *this = *this + o; }
    template<class Vec>
    vec & operator-=(Vec && o) { return *this = *this - o; }
};

template<class T, int N>
struct svec : public std::array<T, N>
{
    svec() {}
    svec(std::initializer_list<T> list)
    {   assert(list.size() <= N);
        std::copy(list.begin(), list.end(), this->begin());
    }
    template<class Vec, class sfinae = decltype(T(vec_elem_t<Vec>()))>
    svec(Vec && o)
    {   *this = forward<Vec>(o);
    }
    template<class Vec, class sfinae = decltype(T(vec_elem_t<Vec>()))>
    svec & operator=(Vec && o)
    {   assert(vec_size(o) == -1 || vec_size(o) == N);
        vec_copy(o, *this, N);
        return *this;
    }
    void resize(int n)
    {   if(n != N) { throw std::runtime_error("svec::resize incorrect"); }
    }
    template<class Vec>
    svec & operator+=(Vec && o) { return *this = *this + o; }
    template<class Vec>
    svec & operator-=(Vec && o) { return *this = *this - o; }
};

// Operator overloading is handled very differently here than in grad.h
// firstly, each scalar element is evaluated on-demand (lazily), and that includes
// chaining through many ops such as `x = a * b + c`

// The reason is much better caching, when not having to write each 
// intermediate op into a large vector/matrix buffer

// Also forward is used so that temporaries passed in are captured by value
// while references/lvalues are captured by reference

// As before, the operator overloads first allow any A and B args,
// but then the return type only compiles for a subset (SFINAE filtering)

template<class A, class B>
using sum_t = decltype(declval<A>() + declval<B>());

template<class V1, class V2>
using elem_sum_t = sum_t<vec_elem_t<V1>, vec_elem_t<V2>>;

template<class F, class... X>
using return_t = decltype(declval<F>()(declval<X>()...));

int both_size(int s1, int s2)
{   if(s1 == -1) { return s2; }
    if(s2 == -1) { return s1; }
    assert(s1 == s2);
    return s1;
}

// collapse pending expression into vec<> object
// useful when accessing each index multiple times
template<class V, vec_size_t<V>* = nullptr>
vec<vec_elem_t<V>> eval(V && v) { return { std::forward<V>(v) }; }

// access each element but ignore value
// useful for map() functors with side-effects
template<class Vec, 
    class E = vec_elem_t<Vec>, class SzT = vec_size_t<Vec>>
void exec(Vec && src, int n = -1)
{   if(n == -1) { n = vec_size(src); }
    for(SzT i=0 ; i<n ; i++) { src[i]; }
}

// // map(f()) to vec api
// template<class F,
//     class R = decltype(declval<F>()(0))>
// struct func_map
// {   F func; int len;
//     int size() const { return len; }
//     R operator[](int i) { return func(i); }
// };
// template<class F>
// func_map<F> map(int size, F && f)
// {   return { forward<F>(f), size };
// }
// template<class F>
// func_map<F> map(F && f)
// {   return { forward<F>(f), -1 };
// }

// map(vec, f(x))
template<class A, class F,
    class R = decltype(declval<F>()(declval<A>()[0]))>
struct vec_map
{   A a; F func;
    int size() const { return vec_size(a); }
    R operator[](int i) { return func(a[i]); }
    R operator[](int i) const { return func(a[i]); }
};
template<class A, class F>
vec_map<A, F> map(A && a, F && f)
{   return { forward<A>(a), forward<F>(f) };
}
// vec >> f(x)
template<class A, class F>
vec_map<A, F> operator>>(A && a, F && f)
{   return { forward<A>(a), forward<F>(f) };
}

// map(vecx, vecy, f(x,y))
template<class A, class B, class F,
    class R = decltype(declval<F>()(declval<A>()[0], declval<B>()[0]))>
struct vec_vec_map
{   A a; B b; F func;
    int size() const { return both_size(vec_size(a), vec_size(b)); }
    R operator[](int i) { return func(a[i], b[i]); }
};
template<class A, class B, class F>
vec_vec_map<A, B, F> map(A && a, B && b, F && f)
{   return { forward<A>(a), forward<B>(b), forward<F>(f) };
}

// vec + vec
template<class A, class B, elem_sum_t<A, B>* = nullptr>
auto operator+(A && a, B && b)
{   return map(forward<A>(a), forward<B>(b), 
        [](auto && ax, auto && bx){ return ax + bx; });
}

// vec - vec
template<class A, class B, elem_sum_t<A, B>* = nullptr>
auto operator-(A && a, B && b)
{   return map(forward<A>(a), forward<B>(b), 
        [](auto && ax, auto && bx){ return ax - bx; });
}

// vec * vec
template<class A, class B, elem_sum_t<A, B>* = nullptr>
auto operator*(A && a, B && b)
{   return map(forward<A>(a), forward<B>(b), 
        [](auto && ax, auto && bx){ return ax * bx; });
}

// vec / vec
template<class A, class B, elem_sum_t<A, B>* = nullptr>
auto operator/(A && a, B && b)
{   return map(forward<A>(a), forward<B>(b), 
        [](auto && ax, auto && bx){ return ax / bx; });
}

// vec * scalar
template<class A, // enable_if kludge to avoid annoying compile error
    std::enable_if_t<!std::is_pointer<std::remove_reference_t<A>>::value>* = nullptr>
auto operator*(A && a, vec_elem_t<A> s)
{   return map(forward<A>(a), [s](auto && ax){ return ax * s; });
}

// vec / scalar
template<class A, // enable_if kludge to avoid annoying compile error
    std::enable_if_t<!std::is_pointer<std::remove_reference_t<A>>::value>* = nullptr>
auto operator/(A && a, vec_elem_t<A> s)
{   assert(s != 0);
    return map(forward<A>(a), [s](auto && ax){ return ax / s; });
}

// element-wise squared
template<class A>
auto squares(A && a)
{   return map(a, [](auto && x){ return x * x; });
}

// dot(vec, vec)
template<class A, class B>
elem_sum_t<A, B> dot(A && a, B && b)
{   int n = both_size(vec_size(a), vec_size(b));
    elem_sum_t<A, B> res = 0;
    for(int i=0 ; i<n ; i++)
    {   res += a[i] * b[i];
    }
    return res;
}

// sum(vec)
template<class A>
vec_elem_t<A> sum(A && a)
{   vec_elem_t<A> res = 0;
    for(int i=0 ; i<a.size() ; i++)
    {   res += a[i];
    }
    return res;
}

// avg(vec)
template<class A>
vec_elem_t<A> avg(A && a)
{   assert(a.size() != 0);
    return sum(a) / a.size();
}

// fill
template<class S>
struct fill_t
{   S val; int n;
    int size() const { return n; }
    S operator[](int) const { return val; }
};
template<class S>
fill_t<S> fill(S && val, int n = -1)
{   return {std::forward<S>(val), n};
}

struct range
{   int n;
    range(int _n) : n(_n) {}
    int size() const { return n; }
    int operator[](int i) { return i; }
};


// slice(vec)
template<class A>
auto slice(A && a, int offset, int len, int stride = 1)
{   assert(vec_size(a) == -1 || vec_size(a) > offset + (len-1)*stride);
    return map(forward<A>(a), [=](int i){ return a[offset + i*stride]; });
}

template<class Stream, class Vec,
    class ElemT = vec_elem_t<Vec>, class SizeT = vec_size_t<Vec>>
Stream & operator<<(Stream && out, Vec const& v)
{   out << "[";
    for(SizeT i=0 ; i<v.size() ; i++)
    {   out << (i ? ", " : "") << v[i];
    }
    return out << "]";
}

} // minilearn