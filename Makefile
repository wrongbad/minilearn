CXX = clang++
CXX_FLAGS = -std=c++14 -I.
# CXX_FLAGS += -Wfatal-errors -Wall -Wextra -Wpedantic -Wconversion -Wshadow 

HEADERS = $(wildcard minilearn/*.h)

TEST_CPP = $(wildcard test/*.cpp)
TEST_NAME = $(TEST_CPP:%.cpp=%)
TEST_BIN = $(TEST_NAME:%=bin/%)

DEBUG_TEST_NAME = $(TEST_NAME:%=debug/%)
DEBUG_TEST_BIN = $(DEBUG_TEST_NAME:%=bin/%)

$(TEST_BIN) : bin/% : %.cpp $(HEADERS)
	mkdir -p $(@D)
	$(CXX) $(CXX_FLAGS) -O3 $< -o $@
	chmod +x $@

$(TEST_NAME) : % : bin/%
	# run the test exe
	$<

tests : $(TEST_NAME)

$(DEBUG_TEST_BIN) : bin/debug/% : %.cpp $(HEADERS)
	mkdir -p $(@D)
	$(CXX) $(CXX_FLAGS) -O0 -g $< -o $@
	chmod +x $@

$(DEBUG_TEST_NAME) : % : bin/%
	# run the test exe
	$<

.PHONY : tests $(TEST_NAME) $(DEBUG_TEST_NAME)